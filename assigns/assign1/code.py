import random, math
import sys,time
import matplotlib.pyplot as plt
x0,k,T,kb=[None]*4
window=10000
def prob_energy(x):
    global k,T,kb,x0
    return math.exp(-(k*(x-x0)**2)/(2*kb*T))
def next_state(x,curr_prob_energy):
    action=random.uniform(-0.5,0.5)
    next_prob_energy=prob_energy(x+action)
    if next_prob_energy<curr_prob_energy:
        action=action if random.uniform(0,1)<(next_prob_energy/curr_prob_energy) else 0
    return x + action, (curr_prob_energy if action == 0 else next_prob_energy)


def monte_carlo(initial_sep,atol=1e-3):
    global delta,window
    curr_prob_energy=prob_energy(initial_sep)
    x=initial_sep
    list_x=[x]
    count=0
    while True:
        count+=1
        x,curr_prob_energy=next_state(x,curr_prob_energy)
        list_x.append(x)
        if count>=window:
            mean1=sum(list_x[-window:-window//2])/(window//2)
            mean2=sum(list_x[-window//2:])/(window//2)
            if count%window==0: print(mean1,mean2)
            if abs(mean1-mean2)<=atol:
                return (mean1+mean2)/2,list_x

def main(x_init,x01,k1,T1,kb1):
    global x0,k,T,kb
    x0,k,T,kb=x01,k1,T1,kb1
    steady_val,list_x= monte_carlo(x_init*x0)
    plt.figure()
    plt.plot(range(len(list_x)),list_x,label="x against #steps")
    plt.plot(range(len(list_x)),[steady_val]*len(list_x),marker=".",label=f"steady value of x={round(steady_val,3)}")
    plt.xlabel("NO of MC steps taken")
    plt.ylabel("x (length of spring)")
    plt.title(f"CONFIGS: initial separation {x_init}, x0={x0}, k={k}")
    plt.legend()
    plt.savefig("part1.png")
    # return steady_val
def plot_simulation_for_different_initsep(initseps,x01,k1,T1,kb1):
    global x0,k,T,kb
    x0,k,T,kb=x01,k1,T1,kb1
    steady_vals,lists_xs=[],[]
    plt.figure()
    for initsep in initseps:
        steady_val,xs=monte_carlo(initsep*x0)
        plt.plot(range(len(xs)),xs,label="x against #steps")
        plt.plot(range(len(xs)),[steady_val]*len(xs),marker=".",label=f"steady value of x={round(steady_val,3)} "
        f"with initial_sep={initsep}")
    plt.xlabel("NO of MC steps taken")
    plt.ylabel("x (length of spring)")
    plt.legend()
    plt.savefig("part1.png")

def plot_simulation_for_different_ks(initsep,x01,ks,T1,kb1):
    global x0,k,T,kb
    x0,T,kb=x01,T1,kb1
    steady_vals,lists_xs=[],[]
    plt.figure()
    for k_ in ks:
        k=k_
        steady_val,xs=monte_carlo(initsep*x0)
        plt.plot(range(len(xs)),xs,label="x against #steps")
        plt.plot(range(len(xs)),[steady_val]*len(xs),marker=".",label=f"steady value of x={round(steady_val,3)} "
        f"with K={k_}")
    plt.xlabel("NO of MC steps taken")
    plt.ylabel("x (length of spring)")
    plt.legend()
    plt.savefig("part2.png")

def plot_simulation_for_different_x0s(initsep,x0s,k1,T1,kb1):
    global x0,k,T,kb
    k,T,kb=k1,T1,kb1
    steady_vals,lists_xs=[],[]
    plt.figure()
    for x0_ in x0s:
        x0=x0_
        steady_val,xs=monte_carlo(initsep)
        plt.plot(range(len(xs)),xs,label="x against #steps")
        plt.plot(range(len(xs)),[steady_val]*len(xs),marker=".",label=f"steady value of x={round(steady_val,3)} "
        f"with x0={x0_}")
    plt.xlabel("NO of MC steps taken")
    plt.ylabel("x (length of spring)")
    plt.legend()
    plt.savefig("part2.png")


def variance(xs,steady_val):
    return sum(list(map(lambda  i : i**2,xs)))/len(xs)-steady_val**2

def plot_equilibrium_for_different_x0s(initsep,x0s,k1,T1,kb1):
    global x0,k,T,kb
    k,T,kb=k1,T1,kb1
    steady_vals,lists_xs=[],[]
    plt.figure()
    for x0_ in x0s:
        x0=x0_
        steady_val,xs=monte_carlo(initsep*x0)
        steady_vals.append(steady_val)
    plt.bar(x0s,steady_vals)
    plt.xlabel("The spring's equilibrium length $x_0$")
    plt.ylabel("<x>")
    for a, b in zip(x0s, steady_vals):
        plt.text(a, b, f"({a},{round(b,3)})")

    plt.title(f"<x> vs $x_0$,  k={k} and init separation={5}$x_0$")
    plt.legend()
    plt.savefig("part2.png")

def plot_variance_for_different_ks(initsep,x01,ks,T1,kb1):
    global x0,k,T,kb
    x0,T,kb=x01,T1,kb1
    steady_vals,lists_xs=[],[]
    vars=[]
    plt.figure()
    for k_ in ks:
        k=k_
        steady_val,xs=monte_carlo(initsep*x0)
        # steady_vals.append(steady_val)
        vars.append(variance(xs,steady_val))
    # plt.hist
    plt.bar(ks,vars)
    plt.xlabel("The spring's constant k")
    plt.ylabel("variance of x")
    for a, b in zip(ks, vars):
        plt.text(a, b, f"({a},{round(b,3)})")

    plt.title(f"variance vs k,  $x_0$={x0} and init separation={5}.$x_0$")
    plt.legend()
    plt.savefig("part3.png")


if __name__=="__main__":
    seed=10
    random.seed(seed)

    # mean_x=main(int(sys.argv[2]),2,1,1,1)
    # vals=list(map(int,sys.argv[2:]))
    # plot_simulation_for_different_initsep(initseps,2,1,1,1)
    # plot_simulation_for_different_ks(10,2,vals,1,1)
    # plot_simulation_for_different_x0s(10,vals,1,1,1)
    main(5,2,1,1,1)
    random.seed(seed)
    plot_equilibrium_for_different_x0s(5,[2,5],1,1,1)
    random.seed(seed)
    plot_variance_for_different_ks(5,2,[0.1,1,10],1,1)
    # print(mean_x)





