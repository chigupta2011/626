import random, math
import sys
import time
import matplotlib.pyplot as plt
# x0,k,T,kb=[None]*4
window=10000
kf,kb=3,1
def kmc():
    del_x=1 if random.uniform(0,1)<kf/(kf+kb) else -1
    del_t=-math.log(random.uniform(0,1))/(kf+kb)

    return del_x,del_t
Tmax=1000
Lmax_factor=10
def simulate(l0,seed):
    Lmax=Lmax_factor*l0
    curr_len,curr_time=l0,0
    xs=[curr_len]
    ts=[curr_time]
    count=0
    while curr_time<=Tmax:
        count+=1
        if count%100==0: print(curr_time)
        del_x,del_t=kmc()
        curr_time+=del_t
        curr_len+=del_x
        xs.append(curr_len)
        ts.append(curr_time)
    return xs,ts
xs_list,ts_list=[],[]
simuls=10
l0=100
xs,ts=simulate(l0,0)
plt.plot(ts,xs)
plt.title(f"$k_f$={kf},$k_b$={kb},$l_0$={l0}")
plt.xlabel("Time-t (sec)")
plt.ylabel("Length of polymer-l (units of monomer)")
plt.savefig("scratch.png")

for seed in range(simuls):
    xs,ts=simulate(l0,seed)
    xs_list.append(xs)
    ts_list.append(ts)
plt.figure()
for i in range(simuls):
    plt.plot(ts_list[i],xs_list[i])
plt.title(f"$k_f$={kf},$k_b$={kb},$l_0$={l0}")
plt.xlabel("Time-t (sec)")
plt.ylabel("Length of polymer-l (units of monomer)")
plt.savefig("diff_simuls.png")

step=0.1
times=[i*step for i in range(int(Tmax/step))]
xs_at_times=[]
for i in range(simuls):
    xs=xs_list[i]
    ts=ts_list[i]
    time=0
    xs_at_time_idx=[]
    for idx in range(len(ts)-1):
        while ts[idx]<=time<=ts[idx+1]:
            xs_at_time_idx.append(xs[idx]+(time-ts[idx])*(xs[idx+1]-xs[idx])/(ts[idx+1]-ts[idx]))
            time+=0.1
    xs_at_times.append(xs_at_time_idx)


meanxs=[]
for i in range(len(times)):
    vals=[x[i] for x in xs_at_times]
    meanxs.append(sum(vals)/len(vals))
plt.figure()
plt.plot(times,meanxs)
plt.title(r"$\langle l \rangle$ vs t "f"$k_f$={kf},$k_b$={kb},$l_0$={l0}")
plt.xlabel("Time-t (sec)")
plt.ylabel(r"$\langle l \rangle$ (units of monomer)")
plt.savefig("<l>_vs_t.png")


dx_dt=[]
window=50
for i in range(len(times)-1):
    min_i=max(0,i-window//2)
    max_i=min(len(times)-1,i+window//2)
    denom=step*(max_i-min_i)
    dx_dt.append((meanxs[max_i]-meanxs[min_i])/denom)
plt.figure()
plt.plot(times[:-1],dx_dt,label=r"$\frac{d\langle l \rangle}{dt}$")
plt.plot(times[:-1],[sum(dx_dt)/len(dx_dt)]*len(dx_dt),label=r"$\langle \frac{d\langle l \rangle}{dt}\rangle=$"
f"{round(sum(dx_dt)/len(dx_dt),3)}")
plt.title(r"$\frac{d\langle l \rangle}{dt}$ vs t "f"$k_f$={kf},$k_b$={kb},$l_0$={l0}")
plt.xlabel("Time-t (sec)")
plt.ylabel(r"$\frac{d\langle l \rangle}{dt}$ ")
plt.legend()
plt.savefig("dldt_vs_t.png")


variances=[]
for i in range(len(times)):
    vals=[x[i] for x in xs_at_times]
    variances.append(sum(map(lambda a:a*a,vals))/len(vals)-meanxs[i]*meanxs[i])
plt.figure()
plt.plot(times,variances)
plt.title(r"$\sigma^2$ vs t "f"$k_f$={kf},$k_b$={kb},$l_0$={l0}")
plt.xlabel("Time-t (sec)")
plt.ylabel(r"$\sigma^2$")
plt.savefig("variance_vs_t.png")


D=[]
window=350
for i in range(len(times)-window):
    min_i=i
    max_i=i+window
    denom=times[max_i]-times[min_i]
    D.append(0.5*(variances[max_i]-variances[min_i])/denom)
plt.figure()
plt.plot(times[:-window],D,label="D vs t")
plt.plot(times[:-window],[sum(D)/len(D)]*len(D),label=f"average D={round(sum(D)/len(D),3)}")
plt.title(f"D vs t  $k_f$={kf},$k_b$={kb},$l_0$={l0}")
plt.xlabel("Time-t (sec)")
plt.ylabel(r"D")
plt.legend()
plt.savefig("dvardt_vs_t.png")
# print(0.5*(variances[-1]-variances[-1000])/(times[-1]-times[-1000]))