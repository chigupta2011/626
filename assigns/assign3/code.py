
import numpy as np
import matplotlib.pyplot as plt
import ray
# ray.init()


# from matplotlib.ticker import FormatStrFormatter
def part1(N=1000):
    np.random.seed(0)
    g_vec=np.empty(N)
    for i in range(N):
        temp=np.random.uniform(size=12)
        g_vec[i]=sum(temp)-6
    mean_g_sq=np.mean(np.square(g_vec))
    w=np.sqrt(mean_g_sq)
    g_vec/=w
    num_bins=10
    fig=plt.figure()
    plt.hist(g_vec,bins=num_bins,density=True)
    plt.xlabel("G")
    plt.ylabel("$P(g=G)$")
    plt.title(f"Prob distrib of the gaussian variable g with \n mean=0 and var=1, w={round(w,3)}")
    plt.savefig("part1.png")
import  math
def prob_energy(x1,x2):
    return math.exp(-(k*(abs(x2-x1)-x0)**2))

def next_state(x1,x2,x0,curr_prob_energy,k,del_t):
    g=np.random.normal()
    f_ext=math.copysign(1,x2-x1)*k*(abs(x2-x1)-x0)
    return x1+del_t*f_ext+math.sqrt(2*del_t)*g,x2
    next_prob_energy=prob_energy(x+action)
    if next_prob_energy<curr_prob_energy:
        action=action if random.uniform(0,1)<(next_prob_energy/curr_prob_energy) else 0
    return x + action, (curr_prob_energy if action == 0 else next_prob_energy)

import random
def langevin(initial_sep=10,x0=2,k=1,atol=1e-3,max_steps=5000,del_t=0.1):
    global delta,window
    x1=0
    x2=initial_sep+x1

    list_x1,list_x2=[x1],[x2]
    count=0
    while True:
        count+=1
        first_particle=0 if random.random()<0.5 else 1
        if first_particle==0:
            x1,x2=next_state(x1,x2,x0,None,k,del_t)
            list_x1.append(x1)
            x2,x1=next_state(x2,x1,x0,None,k,del_t)
            list_x2.append(x2)
        else:
            x2,x1=next_state(x2,x1,x0,None,k,del_t)
            list_x2.append(x2)
            x1,x2=next_state(x1,x2,x0,None,k,del_t)
            list_x1.append(x1)
        if count>max_steps:
            return [abs(list_x1[i]-list_x2[i]) for i in range(len(list_x1))],[i*del_t for i in range(len(list_x1))]

def part3(initial_sep=10,x0=2,k=1,atol=1e-3,max_steps=2000,del_t=0.1,plot=True):
    list_x,list_t= langevin(initial_sep,x0,k,atol,max_steps,del_t)
    mean=sum(list_x[-100:])/100
    if plot:
        plt.figure()
        plt.plot(list_t,list_x,label="$|x_2-x_1|$ vs t")
        plt.plot(list_t,[mean]*len(list_t),linestyle="dashed",linewidth=2, label=r"$|x_2-x_1|_{steady}$="f"{round(mean,3)}")
        plt.xlabel("Time ")
        plt.ylabel("$|x_2-x_1|$ ")
        plt.title(f"CONFIGS: initial separation {initial_sep}, x0={x0}, k={k}")
        plt.legend()
        plt.savefig("part3.png")
    return list_x,list_t,mean
    # return steady_val
def part4(ks,initial_sep=10,x0=2,atol=1e-3,max_steps=2000,del_t=0.1,plot=True):
    xs_k,ts_k,means_k={},{},{}
    for k in ks:
        xs_k[k], ts_k[k], means_k[k]=part3(initial_sep,x0,k,atol,max_steps,del_t,plot=False)
    plt.figure()
    for k in ks:
        plt.plot(ts_k[k], xs_k[k], label=f"$|x_2-x_1|$ vs $t$, $k=${k}")
        plt.plot(ts_k[k], [means_k[k]] * len(ts_k[k]), linestyle="dashed", linewidth=2,
                 label=r"$|x_2-x_1|_{steady}$="f"{round(means_k[k], 3)}, $k=${k}")
    plt.xlabel("Time ")
    plt.ylabel("$|x_2-x_1|$")
    plt.title(f"CONFIGS: initial separation {initial_sep}, x0={x0}")
    plt.legend()
    plt.savefig("part4.png")

def part5(ks,initial_sep=10,x0=2,atol=1e-3,max_steps=1000,del_t=0.1,plot=True):
    xs_k,ts_k,means_k={},{},{}
    vars_k={}
    for k in ks:
        xs_k[k], ts_k[k], means_k[k]=part3(initial_sep,x0,k,atol,max_steps,del_t,plot=False)
        vars_k[k]=sum(map(lambda  x: x*x,xs_k[k][-100:]))/100- (means_k[k])**2
    plt.figure()
    plt.plot(ks, [vars_k[k] for k in ks], label=f"$\sigma^2$ vs $k$")
    plt.xlabel("k")
    plt.ylabel("$\sigma^2$")
    plt.title(f"CONFIGS: initial separation {initial_sep}, x0={x0}")
    plt.legend()
    plt.savefig("part5.png")

# @ray.remote
# def f(i,initial_sep, x0, k, atol, max_steps, del_t):
#     np.random.seed(i)
#     random.seed(i)
#     return langevin(initial_sep, x0, k, atol, max_steps, del_t)


def part6(initial_sep=10,x0=2,k=0,atol=1e-3,max_steps=500,del_t=0.1,nsimuls=5000,plot=True):
    all_xs,ts=[],None
    for i in range(nsimuls):
        np.random.seed(i)
        random.seed(i)
        xs,ts=langevin(initial_sep,x0,k,atol,max_steps,del_t)
        all_xs.append(xs)
    # futures=[f.remote(i,initial_sep, x0, k, atol, max_steps, del_t) for i in range(nsimuls)]
    # all_xs=ray.get(futures)

    all_xs=np.array(all_xs)
    vars_t=np.var(all_xs,axis=0)
    plt.figure()
    plt.plot(ts, vars_t, label=r"$\sigma^2$ vs $t$")
    plt.plot(ts, [vars_t[-1]]*len(ts), linestyle="dashed",label=f"{vars_t[-1]}")
    plt.plot(ts, [vars_t[0]]*len(ts),linestyle="dashed", label=f"{vars_t[0]}")
    plt.xlabel("t")
    plt.ylabel("")
    plt.title(f"CONFIGS: initial separation {initial_sep}, $x_0=${x0}, $k=${k}")
    plt.legend()
    plt.savefig("part6a.png")

    plt.figure()
    slope=[(vars_t[i]-vars_t[i-1])/(del_t) for i in range(1,len(vars_t))]
    plt.figure()
    plt.plot(ts[:-1], slope, label=r"$\frac{\Delta\sigma^2}{\Delta t}$ vs $t$")
    plt.plot(ts[:-1], [np.mean(slope)]*len(ts[:-1]), label=r"$\langle\frac{\Delta\sigma^2}{\Delta t}\rangle=$"f"{round(np.mean(slope),3)}")
    plt.xlabel("t")
    plt.ylabel("")
    plt.title(f"CONFIGS: initial separation {initial_sep}, $x_0=${x0}, $k=${k}, "r"$\langle\frac{d\sigma^2}{dt}\rangle=2$")
    plt.legend()
    plt.savefig("part6.png")

# if __name__=="__main__":


part4([0.1,1,10])

# part4()
